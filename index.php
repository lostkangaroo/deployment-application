<?php

require_once 'vendor/autoload.php';

try {
  $app = new \Deploy\Application();
  $app['debug'] = true;
  $app->run();
}
catch (Exception $e) {
  echo $e->getMessage();
}
