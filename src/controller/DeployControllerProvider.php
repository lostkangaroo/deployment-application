<?php
/**
 * Created by PhpStorm.
 * User: lostkangaroo
 * Date: 5/23/2015
 * Time: 11:09 PM
 */

namespace Deploy\controller;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Response;

class DeployControllerProvider implements ControllerProviderInterface{

  /**
   * Returns routes to connect to the given application.
   *
   * @param Application $app An Application instance
   *
   * @return ControllerCollection A ControllerCollection instance
   */
  public function connect(Application $app)
  {
    $controllers = $app['controllers_factory'];

    $controllers->post('/', function () use ($app) {
      $app['monolog']->addInfo(sprintf("Request from %s", $app['request']->headers->get('User-Agent')));

      try {
        $s = $app['deploy']->execute();
      }
      catch (\Exception $e) {
        $app['monolog']->addError($e->getMessage());
        return new Response("There was an error deploying. Check your logs for more information.", 404);
      }

      $app['monolog']->addInfo(sprintf('%s', $s));
      return $app->json(array($s));
    });

    $controllers->get('/', function () use ($app) {
      $app['monolog']->addInfo(sprintf("Ignoring get request from %s", $app['request']->getClientIp()));
      return '“When 900 years old, you reach… Look as good, you will not.” - Yoda';
    });

    return $controllers;
  }
}
