<?php
/**
 * Based on: http://brandonsummers.name/blog/2012/02/10/using-bitbucket-for-automated-deployments/
 * Modified by lostkangaroo to work with Silex and a few other improvements
 */

namespace Deploy\service\Deploy;

use Deploy\handler\RequestMessageHandlerInterface;
use Deploy\handler\SitesHandler;

date_default_timezone_set('America/Chicago');

/**
 * Class Deploy
 * @package Deploy\service\Deploy
 */
class Deploy {

  /**
   * Deployment configurations
   *
   * @var SitesHandler
   */
  protected $sitesHandler;

  /**
   * Handler to parse request payload
   *
   * @var RequestMessageHandlerInterface
   */
  protected $messageHandler;

  /**
   * @param SitesHandler $sites_handler
   * @param RequestMessageHandlerInterface $message_handler
   */
  public function __construct (SitesHandler $sites_handler, RequestMessageHandlerInterface $message_handler) {
    $this->sitesHandler = $sites_handler;
    $this->messageHandler = $message_handler;
  }

  /**
   * Executes the necessary commands to deploy the site.
   */
  public function execute () {
    $sites = $this->negotiateSites();

    foreach ($sites as $key => $site) {
        $this->deploySite($site);
    }

    return "Deployment was successful.";
  }

  /**
   * Matches site configurations to request information.
   *
   * @return array
   * @throws \Exception
   */
  protected function negotiateSites () {
    $found_sites = array();

    // find any matching site configs for the requested project
    foreach ($this->sitesHandler->getSites() as $key => $site) {
      if ($site['project'] === $this->messageHandler->getProject()) {
        $candidates[$key] = $site;
      }
    }

    // check the branch
    if (!empty($candidates)) {
      $branches = $this->messageHandler->getBranches();

      // find any matching sites based on project and branch and user
      foreach ($candidates as $key => $candidate) {
        if (array_key_exists($candidate['branch'], $branches)) {

          //check the user
          if (!empty($candidate['options']['allowed-users']) && !in_array($this->messageHandler->getUser(), $candidate['options']['allowed-users'])) {
            continue;
          }

          $found_sites[$key] = $candidate;
        }
      }
    }

    if (empty($found_sites)) {
      throw new \Exception("No site configurations available for this request. " . $this->messageHandler->getProject() . " " . serialize($this->messageHandler->getBranches()) . " " . $this->messageHandler->getUser());
    }

    return $found_sites;
  }

  /**
   * Executes deployment console commands
   *
   * @param array $site
   * @throws \Exception
   */
  protected function deploySite ($site) {
    $output = [];

    // go to git repo
    $exec_string  = 'cd ' . $site['repo']['local'] . ' 2>&1 && ';

    // fetch the remote repo
    $exec_string .= 'git fetch -q ' . $site['repo']['remote'] . ' ' . $site['branch'] . ':' . $site['branch'] . ' 2>&1 && ';

    // set HEAD to the branch we are going to update
    $exec_string .= 'git symbolic-ref HEAD refs/heads/' . $site['branch'] . ' 2>&1 && ';

    // update the site based on the new fetch data
    $exec_string .= 'GIT_WORK_TREE=' . $site['working-dir'] . ' git checkout ' . '-f 2>&1 && ';

    // lock down the repo
    $exec_string .= 'chmod -R og-rx ' . $site['repo']['local'] . ' -f 2>&1';

    try {
      exec($exec_string, $output, $value);
    }
    catch (\Exception $e) {
      throw new \Exception("Unable to update site: " . implode('; ', $output));
    }

    if ($value != 0) {
      throw new \Exception("Something went horribly wrong with the deploy cmd string: " . $exec_string . " : " . implode('; ', $output));
    }
  }
}

?>
