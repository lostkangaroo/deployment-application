<?php
/**
 * Created by PhpStorm.
 * User: lostkangaroo
 * Date: 5/24/2015
 * Time: 2:00 AM
 */

namespace Deploy\service;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Deploy\handler\RequestMessageHandler;
use Deploy\handler\SitesHandler;
use Deploy\service\Deploy\Deploy;

class DeployServiceProvider implements ServiceProviderInterface {

  /**
   * Registers services on the given app.
   *
   * This method should only be used to configure services and parameters.
   * It should not get services.
   * @param Application $app
   */
  public function register (Application $app) {
    // holds message payload schemas
    $app['request_message_handler'] = $app->share(function () use ($app) {
      $request_handler = new RequestMessageHandler($app['request']);
      return $request_handler->getHandler();
    });

    // holds deployment configuration settings for sites
    $app['sites_handler'] = $app->share(function () {
      return new SitesHandler(__DIR__);
    });

    // allows for actual deployments to happen
    $app['deploy'] = function () use ($app) {
      return new Deploy($app['sites_handler'], $app['request_message_handler']);
    };
  }

  /**
   * Bootstraps the application.
   *
   * This method is called after all services are registered
   * and should be used for "dynamic" configuration (whenever
   * a service must be requested).
   *
   * @param Application $app
   */
  public function boot (Application $app) {}
}
