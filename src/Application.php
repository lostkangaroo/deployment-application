<?php
/**
 * @file
 *
 * @copyright Copyright (c) 2015 Andrew "lostkangaroo" Jungklaus
 */

namespace Deploy;

use Silex\Application as SilexApplication;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Deploy\controller\DeployControllerProvider;
use Deploy\service\DeployServiceProvider;
use Symfony\Component\HttpFoundation\Response;

class Application extends SilexApplication {

  public function __construct() {
    parent::__construct();

    $this->registerProviders($this);
    $this->registerRoutes($this);
    $this->registerErrors($this);
  }

  protected function registerProviders(Application $app) {
    // Service controller
    $app->register(new ServiceControllerServiceProvider());

    // Load Deploy Services
    $app->register(new DeployServiceProvider());

    // Load logging Service
    $app->register(new MonologServiceProvider(), array('monolog.logfile' => __DIR__ . '/deployments.log',));
  }

  protected function registerRoutes(Application $app) {
    $app->mount('/', new DeployControllerProvider());
  }

  protected function registerErrors(Application $app) {
    $app->error(function (\Exception $e) {
      return new Response($e->getMessage(), 404);
    });
  }
}
