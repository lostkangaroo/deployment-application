<?php
/**
 * Created by PhpStorm.
 * User: lostkangaroo
 * Date: 9/2/2016
 * Time: 1:16 AM
 */

namespace Deploy\handler;

/**
 * Interface RequestMessageHandlerInterface
 *
 * @package Deploy\Handler
 */
Interface RequestMessageHandlerInterface {

  /**
   * RequestMessageHandlerInterface constructor.
   *
   * @param json/string $request_body
   */
  public function __construct($request_body);

  /**
   * Locates project name in request.
   *
   * @return string
   */
  public function getProject ();

  /**
   * Locates any branches from request.
   *
   * @return array
   * @throws \Exception
   */
  public function getBranches ();

  /**
   * Locates the user that triggered the request.
   *
   * @return string
   */
  public function getUser ();
}
