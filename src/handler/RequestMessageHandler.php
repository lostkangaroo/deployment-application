<?php
/**
 * Created by PhpStorm.
 * User: lostkangaroo
 * Date: 9/2/2016
 * Time: 12:36 AM
 */

namespace Deploy\handler;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequestMessageHandler
 * @package Deploy\handler
 */
class RequestMessageHandler {

  /**
   * The handler that is needed for the request host to handle its payload
   *
   * @var RequestMessageHandlerInterface
   */
  public $handler;

  /**
   * RequestMessageHandler constructor.
   * @param Request $request
   * @throws \Exception
   */
  public function __construct ($request) {
    $user_agent = $request->headers->get('User-Agent');
    $class_name = preg_replace('/[^A-Za-z0-9 -]/', '-', trim($user_agent));
    $class_name = preg_replace('/[\\s]+/', '-', $class_name);
    $class_name = strtolower(substr($class_name, 0, strpos($class_name, '-')));
    $class_name = '\\Deploy\\handler\\RequestHandlers\\' . $class_name;

    try {
      $this->handler = new $class_name($request->getContent());
    }
    catch (\Exception $e) {
      throw new \Exception('Unable to read payload, unknown user agent in request headers: ' . $user_agent);
    }
  }

  /**
   * @return \Deploy\Handler\RequestMessageHandlerInterface
   */
  public function getHandler () {
    return $this->handler;
  }
}
?>
