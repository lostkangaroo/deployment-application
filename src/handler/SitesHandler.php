<?php
/**
 * Created by PhpStorm.
 * User: lostkangaroo
 * Date: 5/22/2015
 * Time: 11:42 PM
 */

namespace Deploy\handler;

use Symfony\Component\Yaml\Parser;

class SitesHandler {

  protected $APP_DIR;

  /**
   * Array to store site configurations
   *
   * @var array
   */
  protected $sites = array();

  public function __construct($app_dir) {
    $this->APP_DIR = $app_dir;
    $this->sites = $this->loadSites();
  }

  /**
   * Loads Site Configuration YAML Files
   *
   * @return array
   */
  protected function loadSites() {
    $configs = array();
    $yaml = new Parser();

    //load site configurations
    $files = $this->loadFiles();

    foreach($files as $file){
      $path = $this->APP_DIR . '/../../sites/' . $file;
      $data = $yaml->parse(file_get_contents($path));

      $configs = array_merge($configs, $data);
    }

    return $configs;
  }

  /**
   * @return array
   * @throws \Exception
   */
  protected function loadFiles() {
    $files = array();
    $scaned_files = array_diff(scandir($this->APP_DIR . '/../../sites'), array('..', '.', 'example.yml'));

    if (!empty($scaned_files)) {
      $files = array_merge($files, $scaned_files);
    }
    else {
      throw new \Exception("No Configurations Available");
    }

    return $files;
  }

  /**
   * Returns a site object
   *
   * @param $name
   * @return mixed
   */
  public function getSite($name) {
    return $this->sites[$name];
  }

  /**
   * Returns all site objects
   *
   * @return array
   */
  public function getSites() {
    return $this->sites;
  }
}
