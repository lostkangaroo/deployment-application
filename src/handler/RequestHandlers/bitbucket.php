<?php
/**
 * Created by PhpStorm.
 * User: lostkangaroo
 * Date: 9/2/2016
 * Time: 1:52 AM
 */

namespace Deploy\handler\RequestHandlers;

use Deploy\handler\RequestMessageHandlerInterface;

/**
 * Class bitbucket_org
 * @package Deploy\handler\RequestHandlers
 */
class bitbucket implements RequestMessageHandlerInterface {

  /**
   * @var array
   */
  private $request_body;

  /**
   * bitbucket_org constructor.
   * @param $request_body
   */
  public function __construct ($request_body) {
    $this->request_body = json_decode($request_body);
  }

  /**
   * @inheritDoc
   */
  public function getBranches () {
    $branches = [];

    foreach ($this->request_body->push->changes as $change) {
      if(!isset($change->new->name)) {
        $branches[$change->new->name] = 1;
        continue;
      }

      $branches[$change->new->name]++;
    }

    if(empty($branches)) {
      throw new \Exception("No branch information was included.");
    }

    return $branches;
  }

  /**
   * @inheritDoc
   */
  public function getProject () {
    return $this->request_body->repository->name;
  }

  /**
   * @inheritDoc
   */
  public function getUser () {
    return $this->request_body->actor->username;
  }
}
