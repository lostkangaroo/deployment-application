<?php
/**
 * Created by PhpStorm.
 * User: lostkangaroo
 * Date: 9/2/2016
 * Time: 1:54 AM
 */

namespace Deploy\handler\RequestHandlers;

use Deploy\handler\RequestMessageHandlerInterface;

/**
 * Class github_com
 * @package Deploy\handler\RequestHandlers
 */
class github implements RequestMessageHandlerInterface {

  /**
   * @var array
   */
  private $request_body;

  /**
   * @inheritDoc
   */
  public function __construct($request_body) {
    $this->request_body = json_decode($request_body);
  }

  /**
   * @inheritDoc
   */
  public function GetProject() {
    return $this->request_body->repository->name;
  }

  /**
   * @inheritDoc
   */
  public function GetBranches() {
    $branches = [];

    $ref_array = explode('/', $this->request_body->ref);
    $branch = array_pop($ref_array);

    return $branches[$branch] = 1;
  }

  /**
   * @inheritDoc
   */
  public function GetUser() {
    return $this->request_body->sender->login;
  }
}
