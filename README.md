# README #
Version: 1.0.x

### What is this repository for? ###

* Quickly Deploy Code to Remote Servers using webhooks and a bit of Git magic
* 1.0.x

### How do I get set up? ###

##### Seting up the deployment app #####
* Setup a subdomain on your web server and name it something interesting `deploy.example.com`
* Generate a set of ssh keys to use for deployments without a password named something interesting. `deploy_key` You can reuse keys for multiple domains or create a key for each domain, but only one key per domain.
* Create a .ssh config file if you don't have one already in your .ssh dir to set the `deploy_key` as the default for a specific domain. Something like this is all you need. 
```
Host github.com
  HostName github.com
  IdentityFile ~/.ssh/deploy_key
  User git
```
If you are using multiple central repos your config file could look something like this
```
Host github
  HostName github.com
  IdentityFile ~/.ssh/deploy_key
  User git
  
Host bitbucket
  HostName bitbucket.org
  IdentityFile ~/.ssh/deploy_key
  User git
  
Host central-repo
  HostName central-repo.com
  IdentityFile ~/.ssh/deploy_central_key
  User git
```
For more information on setting up your config file see [ssh config tutorial](https://www.digitalocean.com/community/tutorials/how-to-configure-custom-connection-options-for-your-ssh-client)

* Clone this project into a bare repo somewhere outside of your publicly accessible web folder and name it something interesting. 
`clone --bare git@bitbucket.org:lostkangaroo/deployment-application.git deploy.git`
* Lastly we need to get files into the subdomain directory you set up earlier. Thankfully this is easy enough with a simple git command but must be run from within the newly cloned repo.  `GIT_WORK_TREE=[subdomain/path] git checkout -f` replacing `[subdomain/path]` with the real thing. Absolute Server paths work best here.
* Check the subdomain directory to confirm the project files exist and everything has worked so far.
* Prepare anus for planetary bombardment because things just got easier for you.

##### Setup Site Deployment Configurations #####
* Now that the deploy app is in place we can now make it do all the crazy things like actually deploy your sites. Go to the subdomain folder that we just made sure had all the files in it and your web server can see.
* Go to the Sites directory from the root to start adding config files for you deployments.  These files will be used to define how you use the app going forward for each push to your central repo.
* Now its time to think about how you use Git and your central repos for your projects. Do you use a traditional system of Development / Testing / Production, a simpler Development / Production, or are you a dangerous one and just do your edits directly to Production with a specific branch for each step?  Do you have many branches per feature or a branch per developer on a project? Are all users allowed to push changes to any branch unrestricted or do you monitor who can manage specific branches? 
* Okay now that we have an idea on what our workflows look like lets configure a deployment.  In the sites directory, is an example config file for reference.
* * You will need to create a new file for each solution so lets get busy. Our scenerio will be one developer who uses a development / production workflow but allows outsiders to contribute to his projects.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Special Thanks ###
* All the Shrubbites in the world without them I wouldn't have near the skills to write something like this.
* APQC for coming up with all sorts of odd ball challenges to solve.
* Neclimdul for just being awesome and patient enough to answer noob questions.

Remove me later
